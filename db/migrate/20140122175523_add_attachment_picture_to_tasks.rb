class AddAttachmentPictureToTasks < ActiveRecord::Migration
  def self.up
    change_table :tasks do |t|
      t.attachment :picture
    end
  end

  def self.down
    drop_attached_file :tasks, :picture
  end
end
