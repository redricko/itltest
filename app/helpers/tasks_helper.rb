module TasksHelper
	def read? task
		if cookies['tasks']
	    array = ActiveSupport::JSON.decode(cookies['tasks'])
			array.include?(task.id) ? "Read" : "Not Yet"
		else
			"Not Yet"
		end
  end
end
