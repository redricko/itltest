$(function() {
	$('.task_done').click(function() {
		$(this).parent('form').submit();
	});

  $('#tasks tbody').sortable({
    axis: 'y',
    update: function() {
      $.post(
      	$(this).parent().data('sorting-url'), 
      	$(this).sortable('serialize')
      );
    }
  });
});