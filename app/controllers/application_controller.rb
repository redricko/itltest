class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :done_count

  def done_count
    @done_count = Task.find(:all, conditions: {done: true}).count
  end
end
