class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  # GET /tasks
  def index
    @tasks = Task.order("sequence")
  end

  # GET /tasks/1
  def show
    if cookies['tasks'] 
      array = ActiveSupport::JSON.decode(cookies['tasks'])
    else
      array = Array.new
    end

    unless array.include?(@task.id)
      array << @task.id
    end
    cookies['tasks'] = ActiveSupport::JSON.encode(array)
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  def create
    @task = Task.new(task_params)

    if @task.save
      redirect_to @task, notice: 'Task was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /tasks/1
  def update
    if @task.update(task_params)
      respond_to do |format|
        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.js
      end
    else
      render action: 'edit'
    end
  end

  # DELETE /tasks/1
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
      format.js
    end
  end

  def sorting
    params[:task].each_with_index do |id, index|
      Task.update_all(
        {sequence: index+1}, 
        {id: id}
      )
    end
    render nothing: true
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def task_params
      params.require(:task).permit(:user_id, :name, :done, :picture, :sequence, :'', :category_id)
    end
end
