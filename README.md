## Vítejte v IT Logice

Dostala se Vám do ruky testovací aplikace IT Logica pro nové uchazeče.
Úkolem je zjistit, do jaké míry se orientujete ve světě Ruby on Rails. Nelekejte se pokud některé části této aplikace Vám budou cizí, nebo jim nebudete rozumět. Prostě je přeskočte a jděte na další bod.

Toto je čístá, čerstvě vygenerovaná aplikace. Před samotnou prací je tedy nutné, stejně jako u ostatních aplikací, provést pár stereotipních kroků.

-- 1. V konzoli vstupte do adresáře s aplikací

-- 2. Spusťte instalaci aplikace pomocí příkazu <tt>bundle install</tt>

-- 3. V aplikace je nakonfigurována databáze SQLite. Pro tento účel postačí. Vytvořte tedy databázi příkazem <tt>rake db:create</tt>


## Začněte pracovat krok za krokem.

Doporučuji přečíst si jednotlivé body dopředu. Body, které jdou přímo po sobě, můžete řešit naráz. Body ale neřešte v jiném pořadí. Mezi každým bodem (více body řešené naráz), prosím, proveďte lokální commit do gitu s komentářem které body jsou v něm obsaženy.

Pokud nevíte, jak bod řešit, přeskočte jej, ale již se k němu nevracejte. Validace modelů jsou povinné, nastavujte dle Vaše nejlepšího vědomí a svědomí.

Pokud Vám úkol neurčí jinak, neřešte přihlašování, vlastnictví objektů ani jiné oprávnění.

Neřešte stylování ani jiné designové záležitosti. Aplikace nemusí vypadat dobře. Více nás zajímá kód v controllerech a modelech. 

-- 1. Aplikace by měla možnost spravovat uživatele. Každý uživatel má atributy jméno, příjmení, email, telefon. 

-- 2. Uživatelé se nám rozdělili na dvě skupiny: Administrátor a Manager. Administrace by měla být oddělena. (použijte STI, neřešte oprávnění, kdo co může editovat)

-- 3. Pokud víte co je bootstrap (pokud ne, tento úkol přeskočte), nainstalujte gem twitter-bootstrap-rails, a vytvořte základní layout, a používejte thema pro vytváření dalších pohledů. 

-- 4. Vytvořte jednoduchý TODO list. Musí existovat správa (CRUD - Create Read Update Delete) úkolů, jejich označování jako hotové (atribute 'done'). Bylo by fajn moct pomocí gemu paperclip nebo dragonfly nahrát k úkolu soubor. Pokud si ale nejste uploadem souborů jist, neztrácejte s tím čas.

-- 5. Bylo by fajn, aby se hotové úkoly označovaly jako hotové pomoci Ajaxu

-- 6. Vytvořte změnu pořadí úkolů pomocí Drag&Drop.

-- 7. Úkoly by se měly dát přiřazovat do kategorí. Vytvořte CRUD nad kategoriemi odděleně od úkolů. Ve formuláři úkolu bude možno zvolit existující kategorie. Není třeba vytvářet žádné nové pohledy nebo filtrování. Stačí zobrazit název kategorie jen v přehledu úkolů.

-- 8. pomocí before filtru zobrazujte v layoutu aplikace počet nehotových úkolů.

-- 9. Úkoly by měly být označeny jako přečtené/nepřečtené. Nesouvisí to s tím, zdali je úkol hotový. Realizujte prosím pomocí cookies.

-- 10. Pokud znáte gem devise, použijte ho prosím a vytvořte možnost přihlášení pro administrátory a managery (společně)

-- 11. Každá správná rails aplikace má mít testy. Testy prosím neprogramujte, jen v Rails.root vytvořte soubor <tt>Rails.root.join("testy.txt")</tt> a popište, jaké typy testu byste použili, proč a co byste testovali.

-- 12. Navrhněte další možná rozšíření aplikace a jeho přínosy v souboru <tt>Rails.root.join("rozsireni.txt")</tt>

## Teoretická část

Představte si, že realizujete nástroj na odesílání emailů. 
Pravidelně jednou denně importujete zákazníky a jejich adresty z XML souboru do databáze. Vždy všechny najednou. To znamená, věšechny dosavadní zákazníky z DB smažete a znovu nahrajete všechny z nového souboru.

V nepravidelných intervalech ze seznamu zákazníků odesíláte emaily. Rozeslaní emailů na všechny adresy zabere okolo 3 hodin.

Jak zajistíte, aby rozesílání emailů nebylo narušeno importem nových zákazníků a aby tedy za všech okolností bylo rozeslání kompletní? (neuvažujte transakce ani jiné specifika konkrétních databází. Pracujte prosim na úrovni modelů a přímých vazeb mezi nimi). Import zákazníků nesmí být zpomalen rozesíláním emailů.

Výsledek teroetické části prosím vložte do souboru <tt>Rails.root.join("teorie.txt")</tt>

